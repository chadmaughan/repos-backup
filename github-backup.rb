#!/usr/bin/env ruby

# Chad Maughan
# chadmaughan.com
# 2012-09-24
#
# This is a script for cloning or pulling changes from repositories
# on a scheduled basis from http://github.com
#
# example call (using curl)
#  curl -i "https://api.github.com/users/chadmaughan/repos"

require 'rubygems'
require 'json'
require 'uri'
require 'net/https'
require './lib/cli.rb'

cli = BackupCLI.new
cli.parse_options

user = cli.config[:user]
path = cli.config[:path]

if user.nil? || path.nil?
    puts "Incorrect usage, use '--help' (-h) to see required arguments"
    Process.exit
end

url = URI.parse("https://api.github.com/users/#{user}/repos")
http = Net::HTTP.new(url.host, url.port)
http.verify_mode = OpenSSL::SSL::VERIFY_NONE
http.use_ssl = true
request = http.get url.request_uri

# fix the path if it doesn't end with a '/'
path = /\/$/.match(path) ? path : path + "/"

# parse the response
json = JSON.parse(request.body)

json.each do |repo|

    name = repo['name']
    puts name

    dir = path + name
    Dir.chdir(path)

    # see if the directory exists (update or clone)
    if File.directory?(dir)
        Dir.chdir(dir)
        puts "repository exists, updating to: " + dir
        puts system "git pull"
    else
        puts "repository doesn't exit, cloning to: " + dir
        puts system "git clone git@github.com:#{user}/#{name}.git"
    end

end
