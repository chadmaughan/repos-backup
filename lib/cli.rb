require 'mixlib/cli'

class BackupCLI

  include Mixlib::CLI

  option :user, 
    :short => "-u USER",
    :long  => "--user USER",
	:required => true,
    :description => "User to backup for"

  option :path,
    :short => "-p PATH",
    :long  => "--path PATH",
    :required => true,
    :description => "filesystem path to save or update repositories"

  option :help,
    :short => "-h",
    :long => "--help",
    :description => "Show this message",
    :on => :tail,
    :boolean => true,
    :show_options => true,
    :exit => 0
end
